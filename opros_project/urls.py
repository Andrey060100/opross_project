from django.contrib import admin
from django.urls import path,include
from users.views import HomePage, result


urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/',include('users.urls'
                          '')),
    path('home/', HomePage, name='home'),
    path('',include('django.contrib.auth.urls')),
    path('home/<int:test_id>/', include('questions.urls')),
    path('home/complete/<int:question_index>/', result, name =  'complete')
]
