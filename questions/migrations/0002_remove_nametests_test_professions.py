# Generated by Django 2.1 on 2019-11-01 12:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='nametests',
            name='test_professions',
        ),
    ]
