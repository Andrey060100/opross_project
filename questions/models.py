from django.db import models
from users.models import Profession,CustomUser


class Question(models.Model):
    title = models.CharField(max_length=300, verbose_name='Вопрос')
    topic = models.ForeignKey(Profession, on_delete=models.PROTECT)
    id_test = models.ForeignKey('NameTests', on_delete=models.PROTECT,null=True,default=None)
    def __unicode__(self):
        return self.title, self.topic,self.id_test
    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'
        ordering = ['?']

class Answers(models.Model):
    question_id = models.ForeignKey(Question,on_delete=models.PROTECT, related_name='entries')
    answer = models.CharField(max_length=200, verbose_name='Ответы')
    correct = models.BooleanField(default = False)
    def __str__(self):
        return u'{0}'.format(self.answer)
    class Meta:
        verbose_name_plural = 'Ответы'
        verbose_name = 'Ответ'
        ordering = ['?']

class NameTests(models.Model):
    test_professions = models.ForeignKey(Profession, on_delete=models.PROTECT, blank=True, null = True,verbose_name='Направление вопроса',related_name='entque')
    title_test = models.CharField(max_length=300, verbose_name='Название теста',db_index=True)
    def __str__(self):
        return self.title_test
    def __unicode__(self):
        return  self.test_professions
    class Meta:
       verbose_name_plural = 'Названия Тестов'
       verbose_name = 'Название Теста'

class Results_test(models.Model):
    user_done_test = models.ForeignKey(CustomUser,on_delete=models.PROTECT,verbose_name='Пользователь')
    test = models.ForeignKey(NameTests,on_delete=models.PROTECT,verbose_name='Название теста')
    result = models.PositiveIntegerField()
    class Meta:
       verbose_name_plural = 'Результаты'
       verbose_name = 'Результат'