
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser,Profession
from .forms import CustomUserCreationForm,CustomUserChangeForm

class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ['email','username','profession','is_staff','firstname',
                    'lastname','middlename']

admin.site.register(CustomUser,CustomUserAdmin)
admin.site.register(Profession)
