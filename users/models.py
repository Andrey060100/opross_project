from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomUser(AbstractUser):
    profession = models.ForeignKey('Profession',on_delete=models.PROTECT,verbose_name='Направление', default='Вы не зарегестрированы')
    firstname = models.CharField('Имя', max_length=20)
    lastname = models.CharField('Фамилия', max_length=20)
    middlename =  models.CharField('Отчество', max_length=20)
    class Meta:
        verbose_name_plural = 'ФИО'
        verbose_name = 'ФИО'

class Profession(models.Model):
    vocation = models.CharField(max_length=20,db_index=True, verbose_name='Название')
    def __str__(self):
        return self.vocation
    class Meta:
        verbose_name_plural = 'Направления'
        verbose_name = 'Направление'
        ordering = ['vocation']


