from django.shortcuts import render,get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import CreateView
from .forms import CustomUserCreationForm
from questions.models import Question,Answers,NameTests,Results_test
from users.models import CustomUser
from django.http import HttpResponseRedirect,HttpResponse
from questions.forms import AnswersForm

from datetime import datetime, timedelta

indexes = []
all_estimates = []

class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


def HomePage(request):
    if request.user.is_authenticated:
        current_user = request.user.profession.pk
        global current_user_name
        current_user_name = request.user.pk
        themes = NameTests.objects.filter(test_professions = current_user)
        results = Results_test.objects.all()
        context = {'current_user': current_user,'themes':themes,'results':results}
        return render(request,'home.html',context)
    else:
        return HttpResponseRedirect(reverse_lazy('signup'))

def view_poll(request,test_id):
    global current_test
    current_test = get_object_or_404(NameTests,pk=test_id)
    indexes.clear()
    global buffer
    buffer = test_id
    questions = Question.objects.filter(id_test=current_test)
    for q in questions:
        indexes.append(q.pk)
    question = Question.objects.filter(id_test=current_test).first()
    replies = question.entries.all()
    form = AnswersForm()
    context = {'replies': replies,'question': question, 'form': form,'indexes':indexes,'test_id':test_id}
    return render(request,'questions1.html',context)

def result(request,question_index):
    if request.method == 'POST':
      values = request.POST.getlist('answer')
      e = count_correct_answers(question_index, values)
      all_estimates.append(e)
      delete_first_question(question_index)
      try:
        next_q = indexes[0]
      except IndexError:
          rating = int((sum(all_estimates)/len(all_estimates))*100)
          context = {'rating': rating, 'all_estimates':all_estimates}
          all_estimates.clear()
          udt = CustomUser.objects.get(pk=current_user_name)
          t = current_test
          for r in Results_test.objects.all():
              if r.test == t and r.user_done_test == udt:
                  r = Results_test.objects.get(user_done_test = current_user_name)
                  if rating > r.result:
                        r.result = rating
                  r.save(update_fields = ['result'])

              else:
                  r = Results_test.objects.update_or_create(user_done_test=udt,
                                                            test=t,
                                                            result=rating)
          return render(request, 'result.html', context)
      question = Question.objects.filter(id_test=buffer).get(id=next_q)
      replies = question.entries.all()
      context = {'replies': replies, 'question': question,'values':values,'estimate':e,'all_estimates':all_estimates}
      return render(request, 'questions1.html', context)
    else:
        delete_first_question(question_index)
        next_q = indexes[0]
        question = Question.objects.filter(id_test=buffer).get(id=next_q)
        replies = question.entries.all()
        context = {'replies': replies, 'question': question,}
        return render(request, 'questions1.html', context)

def delete_first_question(question_index):
    indexes.remove(question_index)

def count_correct_answers(question_index,values):
    all_correct_answers = []
    all_choosed_correct_answers = []
    incorrect_answers = []
    question = Question.objects.filter(id_test=buffer).get(id=question_index)
    replies = question.entries.all()
    for r in replies:
        if r.correct == 1:
            all_correct_answers.append(r.answer)
        else:
            continue
    for v in values:
        for correct in all_correct_answers:
            if correct == v:
                all_choosed_correct_answers.append(v)
            else:
                incorrect_answers.append(v)
    if len(incorrect_answers) != 0:
        estimate_for_one_q = len(all_choosed_correct_answers)/(len(all_correct_answers)*2*len(incorrect_answers))
    else:
        estimate_for_one_q = len(all_choosed_correct_answers) / (len(all_correct_answers))
    return estimate_for_one_q

def dtny(request):
    pass



